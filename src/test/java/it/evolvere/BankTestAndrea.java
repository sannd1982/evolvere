package it.evolvere;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class BankTestAndrea {
    private static Bank bank;
    private static Account account1;
    private static Account account2;
    private static Account account3;
    private static Account account4;
    private Map<String, Account> accounts;
    private Map<String, Investimento> investimenti;

    @BeforeClass
    public static void setupBank() {
        bank = new Bank("BNL","utf",100000.9f,2000.8f,7.5f);
        account1 = new Account("Pippo","IT980000021",8000);
        account2 = new Account("Franco","IT980000021",0.0f);
        account3 = new Account("Ermenegildo","IT800000089",1000);
        account4 = new Account("Claretta","IT800000032",2000);


    }

    @Test
    public void verificaAccountEsistenteTramiteIban() {
    bank.addAccount(account4);
    Boolean res = bank.addBalanceToAccount("IT800000032",2000);

    }

    @Test
    public void prelievoNonMaggioredellaDisponibilità (){
        bank.addAccount(account3);
        Boolean res= bank.withdrawFromAccount("IT800000089",1500);
        assertFalse(res);
    }

    //verifica che il balance dell'account sia maggiore di zero
    @Test
    public void verificaBalanceSufficiente(){
        Boolean res= bank.addAccount(account2);
        assertFalse(res);
    }

    //verifico con questo test che gli account sono univoci
    @Test
    public void verificaAccountUnivoci(){
        bank.addAccount(account1);
        bank.addAccount(account3);
        assertFalse(account1.equals(account3));

    }



    @Test
    public void verificoCapitale(){
        bank.addAccount(account1);
        int i= bank.getStatoFinanziario();
        assertEquals(i,3);
    }

    //verifico con questo test che gli account non sono univoci
    @Test
    public void verificoSeIbanSonoUguali(){
        bank.addAccount(account1);
        bank.addAccount(account2);

        assertEquals(account1.getIBAN(), account2.getIBAN());

    }


}
