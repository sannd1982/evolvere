package it.evolvere;

import java.text.ParseException;
import java.util.*;

public class Bank {

    public static final int LIQUIDITA_PRESENTE = 1;
    public static final int COPERTURA_TRAMITE_INVESTIMENTI = 2;
    public static final int COPERTURA_TRAMITE_CAPITALE = 3;
    public static final int BANCAROTTA = 4;

    private Map<String, Account> accounts;
    private Map<String, Investimento> investimenti;

    private String nome;
    private String codice;
    private float capitaleSociale;
    private float liquidita;
    private float commissioneStandard;


    public Bank(String nome, String codice, float capitaleSociale, float bilancioIniziale, float commissioneStandard) {
        this.nome = nome;
        this.codice = codice;
        this.capitaleSociale = capitaleSociale;
        this.liquidita = bilancioIniziale;
        this.accounts = new HashMap<>();
        this.commissioneStandard = commissioneStandard;
        this.investimenti = new HashMap<>();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getCapitaleSociale() {
        return capitaleSociale;
    }

    public void setCapitaleSociale(float capitaleSociale) {
        this.capitaleSociale = capitaleSociale;
    }


    public float getLiquidita() {
        return liquidita;
    }

    public void setLiquidita(float liquidita) {
        this.liquidita = liquidita;
    }

    public String getCodice() {
        return codice;
    }

    public float getCommissioneStandard() {
        return commissioneStandard;
    }

    public void setCommissioneStandard(float commissioneStandard) {
        this.commissioneStandard = commissioneStandard;
    }

    public boolean addAccount(Account account) {
        boolean result = false;

        if (account.getBalance() > 0.0) {
            this.accounts.put(account.getIBAN(), account);
            this.setLiquidita(this.getLiquidita() + account.getBalance());
            result = true;
        }
        return result;
    }

    public boolean addBalanceToAccount(String IBAN, float amount) {
        boolean result = this.accounts.get(IBAN).deposit(amount);
        this.setLiquidita(this.getLiquidita() + amount);
        return result;
    }

    public boolean withdrawFromAccount(String IBAN, float amount) {
        boolean result = this.accounts.get(IBAN).withdraw(amount, this.getCommissioneStandard());
        this.setLiquidita(this.getLiquidita() - amount + this.getCommissioneStandard());
        return result;
    }

    public boolean addInterestToAllAccount() {
        Collection<Account> accounts = this.accounts.values();
        Iterator iterator = accounts.iterator();
        while(iterator.hasNext()){
            ((Account)iterator.next()).addInterest();
        }
        return true;
    }

    public void chiudiInvestimento(String codice){
        Investimento investimento = this.investimenti.get(codice);
        float valoreInvestimento = investimento.chiudiInvestimento();
        this.liquidita += valoreInvestimento;
    }

    public boolean apriInvestimento(String codice, String data, float capitale, float interessi){
        boolean result = false;
        try {
            Investimento nuovoInvestimento = new Investimento(codice, data, capitale, interessi);
            this.investimenti.put(codice, nuovoInvestimento);
            this.liquidita -= capitale;
            result = true;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public float valoreInvestimenti(){
        Collection<Investimento> accounts = this.investimenti.values();
        Iterator iterator = accounts.iterator();
        float totaleInvestimenti = 0.0f;
        while(iterator.hasNext()){
            Investimento currentInvestimento = (Investimento)iterator.next();
            if(currentInvestimento.isActive()){
                totaleInvestimenti += currentInvestimento.getCapitaleAttuale();
            }
        }
        return totaleInvestimenti;
    }

    public float valoreDepositi(){
        Collection<Account> accounts = this.accounts.values();
        Iterator iterator = accounts.iterator();
        float totaleDepositi = 0.0f;
        while(iterator.hasNext()){
            Account currentAccount = (Account) iterator.next();

                totaleDepositi += currentAccount.getBalance();
            }
        return totaleDepositi;
    }

    public int getStatoFinanziario(){
        int stato;
        float depositi = this.valoreDepositi();
        if(this.liquidita - depositi > 0){
            stato = Bank.LIQUIDITA_PRESENTE;
        }else if(this.liquidita + this.valoreInvestimenti() - depositi > 0){
            stato = Bank.COPERTURA_TRAMITE_INVESTIMENTI;
        }else if(this.liquidita + this.valoreInvestimenti() + this.capitaleSociale - depositi > 0){
            stato = Bank.COPERTURA_TRAMITE_CAPITALE;
        }else{
            stato = Bank.BANCAROTTA;
        }
        return stato;
    }



}
