package it.evolvere;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Investimento {
    private String codice;
    private Date dataInizioInvestimento;
    private float capitale;
    private float interessiGiornalieri;
    private boolean active;


    public Investimento(String codice, String dataInizioInvestimentoString, float capitale, float interessiGiornalieri) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN);
        this.dataInizioInvestimento = sdf.parse(dataInizioInvestimentoString);
        this.capitale = capitale;
        this.active = true;
        this.interessiGiornalieri = interessiGiornalieri;
        this.codice = codice;
    }

    public float getCapitaleAttuale(){
        Date today = new Date();
        long diffInMillies = Math.abs(today.getTime() - this.dataInizioInvestimento.getTime());
        long giorniMaturati = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        float interessiMaturati;
        for (int i = 0; i <= giorniMaturati; i++ ){
            interessiMaturati = this.capitale * interessiGiornalieri;
            this.capitale += interessiMaturati;
        }
        return capitale;
    }

    public float chiudiInvestimento() {
        this.active = false;
        return this.getCapitaleAttuale();
    }


    public boolean isActive() {
        return active;
    }
}
